#include <cstdlib>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <ctime>
#include "mpi.h"
#include "regex.h"
#include <sstream>
#include <vector>
#include <map>

using namespace std;

struct range {
  int left;
  int right;
};

int count_lines() {
  int numLines = 0;
  ifstream in("log");
  std::string unused;
  while (std::getline(in, unused))
     ++numLines;

  return numLines;
}

std::vector<std::string> parse_line(string line) {
  stringstream iss(line);
  std::vector<std::string> tokens; do {
    std::string sub;
    iss >> sub;
    tokens.push_back(sub);
  } while (iss);

  return tokens;
}

std::map<std::string, int> read_file(int start, int end) {
  ifstream iFile("log");
  string line;
  std::map<std::string, int> m;

  int current_line;

  while (getline(iFile, line)) {
    if(current_line < start) {
      current_line++;
      continue;
    }

    if(current_line == end) {
      break;
    }

    current_line++;

    std::vector<std::string> s;
    s = parse_line(line);
    m[s.at(0)]++;

  }

  iFile.close();

  return m;
}


int main(int argc, char *argv[]) {
  int id;
  int p;
  double wtime;

  MPI::Init(argc, argv);
  p = MPI::COMM_WORLD.Get_size();
  id = MPI::COMM_WORLD.Get_rank();

  if (id == 0) {
    int lines = count_lines();

    cout << "Plik ma " << lines << " linii" << endl;

    std::map<std::string, int> m;
    std::map<std::string, int>::iterator iter;
    m = read_file(0, lines);

    int lines_per_process = lines / p;
    int rest_lines = lines % p;

    cout << "Kazdy proces bedzie mial " << lines_per_process << " do przetworzenia " << endl;

    vector<range> line_ranges;
    int current_line = 0;

    for(int i = 0; i < p; i++) {
      range r = { current_line, current_line + lines_per_process};

      line_ranges.push_back(r);

      current_line += lines_per_process;
    }

    line_ranges.at(p - 1).right += rest_lines;

    for(int i = 0; i < p; i++) {
      cout << "(" << line_ranges[i].left << line_ranges[i].right << ")" << endl;
    }

    for(int i = 0; i < p; i++) {
      int r[2];

      r[0] = line_ranges[i].left;
      r[1] = line_ranges[i].right;

      MPI::COMM_WORLD.Send(&r, 2, MPI::INT, i, 0);
    }

    for(iter = m.begin(); iter != m.end(); iter++) {
      cout << iter->first << " -->" << iter->second << endl;
    }
  } else {
    cout << "Halo ryba" << endl;

    int r[2];

    MPI::COMM_WORLD.Recv(&r, 2, MPI::INT, 0, MPI_ANY_TAG);
    cout << "(" << r[0] << "," << r[1] << ")" << endl;
  }

  MPI::Finalize();

  return 0;
}
