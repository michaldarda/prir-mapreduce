#include <cstdlib>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <regex>
#include <ctime>
#include "mpi.h"

using namespace std;

void parse_line(string line) {
  regex logline("\\(\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2})(:\\d*,\\d*)\\s(\\d*.\\d*.\\d*.\\d)\\");
  smatch result;

  if (regex_search(line, result, logline)) {
    ;
  }
}

void read_file() {
  ifstream iFile("log");
  string line;

  while (getline(iFile, line)) {
    parse_line(line);
  }

  iFile.close();
}


int main(int argc, char *argv[]) {
  int id;
  int p;
  double wtime;

  MPI::Init(argc, argv);
  p = MPI::COMM_WORLD.Get_size();
  id = MPI::COMM_WORLD.Get_rank();

  if (id == 0) {
    cout << "\n";
    cout << "HELLO_MPI - Master process:\n";
    cout << "  C++/MPI version\n";
    cout << "  An MPI example program.\n";
    cout << "\n";
    cout << "  The number of processes is " << p << "\n";
    cout << "\n";
  }

  if (id == 0) {
    wtime = MPI::Wtime();
  }

  cout << "  Process " << id << " says 'Hello, world!'\n";

  if (id == 0) {
    wtime = MPI::Wtime() - wtime;
    cout << "  Elapsed wall clock time = " << wtime << " seconds.\n";
  }

  read_file();

  MPI::Finalize();

  if (id == 0) {
    cout << "\n";
    cout << "HELLO_MPI:\n";
    cout << "  Normal end of execution.\n";
    cout << "\n";
  }
  return 0;
}
