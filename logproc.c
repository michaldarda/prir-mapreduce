#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mpi.h"

// funkcja liczy linie w pliku
int count_lines(char* filename) {
    FILE* file = fopen(filename, "r");

    int lines = 0;
    char line[128];
    while (fgets(line, sizeof(line), file)) {
        lines++;
    }

    return lines;
}

int main(int argc, char** argv) {
    int numprocs, myid;

    MPI_Init(&argc, &argv);

    MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
    MPI_Comm_rank(MPI_COMM_WORLD,&myid);

    // proces glowny
    // proces glowny nie oblicza nic
    // tylko scala wyniki (wykonuje operacje reduce)
    if (myid == 0) {
        // liczymy linie w pliku
        int lines = count_lines("log");
        printf("%d", lines);

        // srednia liczba linii na proces
        int lines_per_process = lines / (numprocs - 1);
        printf("%d", lines_per_process);
        int rest = lines % (numprocs - 1);

        // przedzialy czyli np proces 1 bedzie przerabial plik od linii 0 do 2
        int** line_ranges = malloc(numprocs* sizeof(int*));

        int current_line = 0;

        // dzielimy plik na przedzialy linii (patrz wyzej)
        // zwroc uwage ze jest od 1, to dlatego ze 1 proces nie wykonuje obliczen
        // dalej kazda petla zaczynajaca sie od 1 jest z takiego samego powodu
        for (int i = 1; i < numprocs; i++) {
            line_ranges[i] = malloc(2 * sizeof(int)); line_ranges[i][0] = current_line;
            line_ranges[i][1] = current_line + lines_per_process;

            current_line += lines_per_process;
        }

        // i dodajemy pozostale linie do ostatniego przedzialu
        line_ranges[numprocs - 1][1] += rest;

        // wypisuje linie zeby zobaczyc czy dobrze podzielilem
        for (int i = 1; i < numprocs; i++) {
            printf("(%d,%d)\n", line_ranges[i][0], line_ranges[i][1]);
        }

        // wysylam przedzialy do procesow
        for (int i = 1; i < numprocs; i++) {
            MPI_Send(line_ranges[i], 2, MPI_INT, i, 99, MPI_COMM_WORLD);
        }

        // tutaj zaczyna sie odbior danych
        MPI_Status status;

        // Operacja REDUCE

        // tutaj trzymamy wszystkie wyniki, juz glowne
        // w rootKeys trzymamy klucze (wg ktorych grupujemy)
        // w rootValues trzymamy wartosci (ile razy wystepuje dana linia grupowana wedlug klucza)
        char** rootKeys = malloc(lines * sizeof(char*));
        int* rootValues = malloc(lines * sizeof(int));
        int current_values_size = 0;

        // odbieramy dane od kazdego z procesow
        for(int i = 1; i < numprocs; i++) {
            char key[256];
            char value[256];

            int already_added = 0;

            for (int j = 0; j < (line_ranges[i][1] - line_ranges[i][0]); j++) {
                // proces potomny wysyla dane linia po linii, tak tez odbieramy
                MPI_Recv(key, 256, MPI_CHAR, i, 101, MPI_COMM_WORLD, &status);
                MPI_Recv(value, 256, MPI_CHAR, i, 101, MPI_COMM_WORLD, &status);

                // tutaj grupujemy takie same wartosci do jednego klucza
                // zeby nie zdarzylo sie tak ze beda dwa klucze o takiej samej nazwie
                for (int k = 0; k < current_values_size; k++) {
                    // wyzej wymieniony warunek zachodzi, znajdz klucz i zwieksz krotnosc klucza
                     if (strcmp(rootKeys[k], key) == 0) {
                         rootValues[k]++;
                         already_added = 1;
                         break;
                     }
                }

                // klucz jeszcze nie istnieje, dopisz na koniec
                if(!already_added) {
                     rootKeys[current_values_size] = malloc(256 * sizeof(char));
                     strcpy(rootKeys[current_values_size], key);
                     rootValues[current_values_size] = 1;
                     current_values_size++;
                 }
            }
        }

        for (int i = 0; i < current_values_size; i++) {
            printf("%s => %d\n", rootKeys[i], rootValues[i]);
        }
    } else {
        // Operacja map
        MPI_Status status;
        int* resuls = malloc(2 * sizeof(2));
        // odbierz dane od glownego
        MPI_Recv(resuls, 2, MPI_INT, 0, 99, MPI_COMM_WORLD, &status);

        FILE* file = fopen("log", "r"); char line[128];

        // odczytaj przedzial ktora ma policzyc
        int start = resuls[0];
        int end = resuls[1];

        // klucze i wartosci dla wynikow czastkowych
        // wartosci to cale linie
        char** keys = malloc((end-start)* sizeof(char*));
        char** values = malloc((end-start)* sizeof(char*));
        int values_count = 0;

        for(int i = 0; i < (end-start); i++) {
            keys[i] = malloc(256 * sizeof(char));
            values[i] = malloc(256 * sizeof(char));
        }

        int current_line = 0;

        //czytamy plik
        while (fgets(line, sizeof(line), file)) {
            // jezeli linia to nie start
            // to poczekaj na linie
            if(current_line < start) {
                current_line++;
                continue;
            }

            // skoncz jezeli koniec przedzialu
            if(current_line == end) { break; }

            strcpy(values[values_count], line);

            char* tokens[10];
            int i = 0;
            // dzielenie linii na tokeny
            // jezeli chcemy przestawic grupowanie to tutaj
            tokens[i] = strtok(line, " \n");

            char* datetime;
            char* hour;
            char* minute;
            char* second;
            char* ip_adress;

            while (tokens[i] != NULL) {
                tokens[++i] = strtok(NULL, " : , \n");
            }

            datetime = tokens[0];
            hour = tokens[1];
            minute = tokens[2];
            ip_adress = tokens[5];

            // jezeli chcemy grupowac wedlug innej wartosci
            // wystarczy skonstruowac inny napis wedlug ktorego mozemy grupowac
            // np mozemy podac ip_adress
            strcpy(keys[values_count], strcat(strcat(datetime, hour), minute));

            values_count++;
            current_line++;
        }

        // mozna odkomentowac i ladnie wypisze wynik czastkowy operacji map
        /* for(int i = 0; i < (end - start); i++) { */
        /*     printf("%s => %s", keys[i], values[i]); */
        /* } */

        // wyslij dane linia po linii do glownego
        for(int i = 0; i < (end - start); i++) {
            MPI_Send(keys[i], 256, MPI_CHAR, 0, 101, MPI_COMM_WORLD);
            MPI_Send(values[i], 256, MPI_CHAR, 0, 101, MPI_COMM_WORLD);
        }
    }

    MPI_Finalize();
}
