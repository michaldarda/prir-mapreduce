#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

typedef struct
{
    char *key;
    char *value;
} dict_t;

int
count_lines (char **filename)
{
    int ch;
    int lines = 0;

    while (EOF != (ch = getchar ()))
        if (ch == '\n')
            ++lines;

    return lines;
}

dict_t
parse_line(char* line)
{
}

void
parse_file (char** filename)
    FILE * fp;
    char * line = NULL;
    size_t len = 0;
    ssize_t read;
    dict_t parsed_line;

    int lines = count_lines(filename)

    dict_t* parsed_lines = malloc(lines * sizeof(dict_t))
    int current_line = 0;

    while ((read = getline(&line, &len, fp)) != -1) {
        parsed_line = parse_line(line)
        parsed_lines[0] = parsed_line
        current_line++;
    }
}

int
main (int argc, char **argv)
{
    MPI_Init (argc, &argv);

    MPI_Finalize ();
    return EXIT_SUCCESS;
}
